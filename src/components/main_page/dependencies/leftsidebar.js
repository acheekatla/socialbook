import React from "react";

const ImportantLinks = () => {
  return (
    <div className="imp-links">
      <a href="/">
        <img src="https://i.postimg.cc/RCj4MjnC/news.png" alt="" />
        Latest News
      </a>
      <a href="/">
        <img src="https://i.postimg.cc/MpBwMtV0/friends.png" alt="" />
        Friendss
      </a>
      <a href="/">
        <img src="https://i.postimg.cc/44FRWj1b/group.png" alt="" />
        Group
      </a>
      <a href="/">
        <img src="https://i.postimg.cc/0jh39RtT/marketplace.png" alt="" />
        Marketplace
      </a>
      <a href="/">
        <img src="https://i.postimg.cc/VsXHCTVk/watch.png" alt="" />
        Watch
      </a>
      <a href="/">See More</a>
    </div>
  );
};

class LeftSideBar extends React.Component {
  shortcut_data = [
    {
      name: "Web Developers",
      link: "https://i.postimg.cc/3JHVf7vG/shortcut-1.png",
    },
    {
      name: "Web Design Course",
      link: "https://i.postimg.cc/rFCbvb1P/shortcut-2.png",
    },
    {
      name: "Full Stack Development",
      link: "https://i.postimg.cc/0yk3xfZ2/shortcut-3.png",
    },
    {
      name: "Website Experts",
      link: "https://i.postimg.cc/Z5wQqdDP/shortcut-4.png",
    },
    {
      name: "Web Waste Fellows",
      link: "https://i.postimg.cc/3JHVf7vG/shortcut-1.png",
    },
    {
      name: "Quarter Stack Development",
      link: "https://i.postimg.cc/rFCbvb1P/shortcut-2.png",
    },
  ];

  render() {
    return (
      <div className="left-sidebar">
        <ImportantLinks />
        <div className="shortcut-link">
          <p>Your Shortcuts</p>
          {this.shortcut_data.map((data) => {
            return (
              <a href="/">
                <img src={data.link} alt={data.name} />
                {data.name}
              </a>
            );
          })}
        </div>
      </div>
    );
  }
}

export default LeftSideBar;
