import React from "react";

import "./index.css";
import NavBar from "../common/Navbar";
import Footer from "../common/footer";
import LeftSideBar from "../main_page/dependencies/leftsidebar";
import RightSideBar from "../main_page/dependencies/rightsidebar";
import MainContent from "../main_page/dependencies/maincomponent";

/**
 * This is a class representing the main page of the application.
 */
class Main extends React.Component {
  render() {
    return (
      <>
        <NavBar />
        <div className="container">
          <LeftSideBar />
          <MainContent />
          <RightSideBar />
        </div>
        <Footer />
      </>
    );
  }
}

export default Main;

